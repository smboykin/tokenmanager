var surveymanagerapp = angular.module('surveymanager', ['ui.router', 'ngResource']);

surveymanagerapp.config(function ($httpProvider, $stateProvider, $urlRouterProvider) {
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

    $stateProvider
        .state('home', { //landing page
            url: '/admin',
            templateUrl: 'partials/home.html',
            controller: 'home'
        }).state('projects', {
            url: '/admin/projects',
            params: {
                appMessage: ""
            },
            templateUrl: 'partials/projects.html',
            controller: 'ProjectListController'
        }).state('newProject', {
            url: '/admin/projects/new',
            templateUrl: 'partials/project_add.html',
            controller: 'ProjectCreateController'
        }).state('viewProject', {
            url: '/admin/projects/:id/view',
            templateUrl: 'partials/project_view.html',
            controller: 'ProjectViewController'
        }).state('editProject', { // show projects
            url: '/admin/projects/:id/edit',
            templateUrl: 'partials/project_edit.html',
            controller: 'ProjectEditController'
        });
}).run(function ($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
});
