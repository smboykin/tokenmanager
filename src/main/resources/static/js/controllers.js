/**
 * Created by sboykin on 4/24/2015.
 * TODO refactor authentication functionality into a org.chip.ihl.surveymanager.service
 */

function resetMessages(scope) {
    scope.errorMessage = false;
    scope.appMessage = "";
}
//angular.module('surveymanager.controllers', [])
surveymanagerapp.controller('LoginController', function LoginController($rootScope, $scope, AuthService, $window) {
    $scope.credentials = {};
    $scope.login = function() {
        console.log('AuthService: ' + AuthService)
        AuthService.login($scope.credentials).then(function (authenticated) {
            console.log("Login succeeded")
            $rootScope.errorMessage = false;
            $rootScope.authenticated = true;
            $window.location.href = '/';
        }, function (response) {
            console.log("Login failed")
            $rootScope.errorMessage = 'There was a problem logging in. Please try again.';
            $rootScope.authenticated = false;
        });
    };

});
surveymanagerapp.controller('NavigationController', function NavigationController($rootScope, $scope, $state, AuthService, $window) {
    resetMessages($rootScope);
        $scope.logout = function() {
            AuthService.logout().then(function() {
                $rootScope.errorMessage = false;
                $rootScope.appMessage = 'You have been logged out.';
                $window.location.href = '/login.html';
            }, function(response) {
                $rootScope.errorMessage = response;
            });
        };
    $state.go('home');
    }).controller('home', function($rootScope) {
        resetMessages($rootScope);
    }).controller('ProjectListController',
    function ProjectListController($rootScope, $scope, $state, $stateParams, Project, popupService) {
        resetMessages($rootScope);
        $rootScope.appMessage = $stateParams.appMessage;
        $scope.projects = [];
        Project.query(function(response) {
            $scope.projects = response ? response : [];
            console.log('Fetch projects succeeded')
        }, function() {
           $scope.projects = [];
            $rootScope.errorMessage = 'An error occurred attempting to retrieve REDCap projects from server. Please check application logs.';
            console.log($rootScope.errorMessage)
        });

        $scope.deleteProject = function(project) {
            if (popupService.showPopup('Delete this project?')) {
                console.log('Going to try to delete project')
                project.$delete(function() {
                    var message = "Project '"+ project.name + "' has been deleted.";
                    console.log(message)
                    $state.go('projects', {appMessage: message}, {reload: true});
                }, function() {
                    $rootScope.errorMessage = 'An error occurred attempting to delete project: ' + project.id;
                    console.log($rootScope.errorMessage)
                });
            }
        }
    }).controller('ProjectCreateController', function ProjectCreateController($rootScope, $scope, $state, $stateParams, Project, formService) {
        resetMessages($rootScope);
        $scope.server = {};

        $scope.project = new Project();
        $scope.newForm = {value: ""};
        $scope.project.errors = {};

        $scope.addForm = function(formvalue) {
            if (formvalue) {
                $scope.project.forms = formService.addForm($scope.project.forms, formvalue);
                $scope.newForm.value = "";
            }
        };
        $scope.deleteForm = function(index) {
            $scope.project.forms = formService.deleteForm($scope.project.forms, index);
        }

        $scope.addProject = function() {
            delete $scope.project.errors;
            $scope.project.$save(function() {
                var message = "Successfully added new project '" + $scope.project.name + "'.";
                $state.go('projects', {appMessage: message}, {reload: true});
                console.log(message)
            }, function(response) {
                $rootScope.errorMessage = 'An error occurred attempting to add project';
                console.log($rootScope.errorMessage)
                if (response.status === 400) {
                    $scope.project.errors = response.data;
                }

            });
        }
    }).controller('ProjectViewController', function ProjectViewController($rootScope, $scope, $stateParams, Project) {
        resetMessages($rootScope);
        $scope.project = {};
        Project.get({ id: $stateParams.id }, function(project) {
            $scope.project = project;
        }, function() {
            $rootScope.errorMessage = 'An error occurred attempting to get project from server.';
            console.log($rootScope.errorMessage)
        });
    }).controller('ProjectEditController', function ProjectEditController($rootScope, $scope, $state, $stateParams, Project, formService) {
        resetMessages($rootScope);
        $scope.newForm = {value: ""};
        $scope.updateProject = function() {
            delete $scope.project.errors;
            $scope.project.$update(function() {
                var message = "Project '" + $scope.project.name + "' successfully updated.'";
                console.log(message)
                $state.go('projects', {appMessage: message}, {reload: true});
            }, function(response) {
                $rootScope.errorMessage = "An error occurred attempting to update project '" + $scope.project.name + "'.";
                console.log($rootScope.errorMessage)
                if (response.status === 400) {
                    $scope.project.errors = response.data;
                }
            });
        };

        $scope.loadProject = function() {
            $scope.project = {};
            Project.get({ id: $stateParams.id }, function(project) {
                $scope.project = project;
                $scope.displayName = project.name;
                $rootScope = false;
            }, function() {
                $rootScope.errorMessage = 'An error occurred attempting to get project from server.';
                console.log($rootScope.errorMessage)
            });
        };

        $scope.addForm = function(formvalue) {
            if (formvalue) {
                $scope.project.forms = formService.addForm($scope.project.forms, formvalue);
                $scope.newForm.value = "";
            }
        };

        $scope.deleteForm = function(index) {
            $scope.project.forms = formService.deleteForm($scope.project.forms, index);
        }

        $scope.loadProject();
    });
