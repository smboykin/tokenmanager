/**
 * Created by sboykin on 5/6/2015.
 */
surveymanagerapp.directive('newformd', function() {
   return {
       require: 'ngModel',
       link: function(scope, elm, attrs, ctrl) {
           ctrl.$validators.newformd = function(modelValue, viewValue) {
               if (ctrl.$isEmpty(modelValue)) {
                   // ok if empty
                   return true;
               }
               // not ok if already in list
               var forms = scope.project.forms;
               if (forms.indexOf(modelValue) === -1) {
                   return true;
               } else {
                   return false;
               }
           }
       }
   }
});