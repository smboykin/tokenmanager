/**
 * Created by sboykin on 4/24/2015.
 *
 * Services to access REST API calls
 */
// REDCap project services
surveymanagerapp.factory('Project', function($resource) {
    var object = $resource('/redprojects/:id', {id: '@id'}, {
        update: {
            method: "PUT"
        }
    });
    object.prototype.forms = [];
    return object;
});

// Project form service - for adding and deleting project forms
surveymanagerapp.service('formService', function() {
    this.deleteForm = function(forms, index) {
        forms.splice(index, 1);
        return forms;
    };

    this.addForm = function(forms, form) {
        if (!forms) {
            forms = [];
        }
        forms.push(form);
        return forms;
    }

});

// Simple popup service
surveymanagerapp.service('popupService',function($window) {
    this.showPopup = function (message) {
        return $window.confirm(message);
    }
});

// Authentication
surveymanagerapp.service('AuthService',function($http, $q, $window) {
   return {
       login: function(credentials) {
           var deferred = $q.defer();
           var postData = "";
           if (credentials) {
               var uname = credentials.username ? credentials.username : '';
               var pwd = credentials.password ? credentials.password : '';
               postData = "username="+ uname + "&password=" + pwd;
           }
           $http({
               method: 'POST',
               url: '/auth',
               data: postData,
               headers: {
                   "Content-Type": "application/x-www-form-urlencoded",
                   "X-Login-Ajax-call": 'true'
               }
           })
               .then(function(response) {
                   if (response.status === 200) {
                       deferred.resolve("Successfull");
                   } else {
                       deferred.reject(response);
                   }

               }, function(response) {
                   deferred.reject(response);
               })
           return deferred.promise;
       },
       logout: function() {
           var deferred = $q.defer();
           $http.post('/logout')
               .then(function(response) {
                   if (response.status === 200) {
                       deferred.resolve('Ok');
                      // $window.location.reload();
                   } else {
                       deferred.reject('Error logging out. Check server logs.');
                   }
           });
           return deferred.promise;
       }
   }
});