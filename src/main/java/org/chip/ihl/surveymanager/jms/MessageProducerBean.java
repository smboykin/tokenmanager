/*
 * Copyright (c) 2014, Boston Children's Hospital. All Rights Reserved.
 *
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */

package org.chip.ihl.surveymanager.jms;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

import javax.jms.*;

/**
 * Bean to send messages to queue
 * Created by sboykin on 11/25/2014.
 *
 * TODO setup message sends to be asynchronous (to allow for longer timeouts)
 */
@Service
public class MessageProducerBean {
    private final Logger logger = LoggerFactory.getLogger(MessageProducerBean.class);
    private final JmsTemplate jmsTemplate;
    private final ObjectMapper objectMapper;

    @Autowired
    public MessageProducerBean(JmsTemplate jmsTemplate, ObjectMapper objectMapper) {
        this.jmsTemplate = jmsTemplate;
        this.objectMapper = objectMapper;
    }

    /**
     * sends out a message (in JSON format)
     * @param surveyMessage the message to send
     */
    public void sendMessage(String destination, final SurveyMessage surveyMessage) {
        jmsTemplate.send(destination, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                try {
                    String textAsJson = "";
                    TextMessage textMessage = session.createTextMessage();
                    if (surveyMessage != null && surveyMessage.getRecords() != null) {
                        textAsJson = objectMapper.writeValueAsString(surveyMessage.getRecords());
                        textMessage.setText(textAsJson);
                    }
                    logger.info("Sending the following text: " + textAsJson);
                    return textMessage;
                } catch (JsonProcessingException e) {
                    logger.error("Unable to convert message to json format", e);
                    throw new JMSException("Unable to convert message to json format");
                }
            }
        });
    }
}
