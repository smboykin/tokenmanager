package org.chip.ihl.surveymanager;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by sboykin on 5/11/2015.
 * Authentication success handler for AJAX calls
 */
public class AjaxAuthenticationFailureHandler implements AuthenticationFailureHandler {
    private AuthenticationFailureHandler defaultHandler;

    public AjaxAuthenticationFailureHandler(AuthenticationFailureHandler defaultHandler) {
        this.defaultHandler = defaultHandler;
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        if ("true".equals(httpServletRequest.getHeader("X-Login-Ajax-call"))) {
            httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        } else {
            defaultHandler.onAuthenticationFailure(httpServletRequest, httpServletResponse, e);
        }
    }
}
