/*
 * Copyright (c) 2015, Boston Children's Hospital. All Rights Reserved.
 *
 * Developed by the Intelligent Health Lab at Children's Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.chip.ihl.surveymanager.tokenmanager.controller;

import org.chip.ihl.surveymanager.controller.BaseController;
import org.chip.ihl.surveymanager.exception.ResourceNotFoundException;
import org.chip.ihl.surveymanager.redcap.RedcapProject;
import org.chip.ihl.surveymanager.tokenmanager.RedcapProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.trace.TraceRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * RESTful front end for CRUD on Redcap project metadata
 * Created by sboykin on 05/01/2015.
 */
@RestController
@RequestMapping("/redprojects")
public class RedcapProjectController extends BaseController {
    private RedcapProjectRepository repository;

    @Autowired
    public RedcapProjectController(RedcapProjectRepository projectRepository, TraceRepository traceRepository) {
        super(traceRepository);
        this.repository = projectRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List findProjects() {
        return repository.findAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public RedcapProject addProject(@RequestBody @Valid RedcapProject project) {
     traceAction(String.format("create REDCap project, alias: %s", project.getName()));
        project.setId(null);
        return repository.saveAndFlush(project);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public RedcapProject updateProject(@RequestBody @Valid RedcapProject updatedProject, @PathVariable Integer id) {
        traceAction(String.format("update REDCap project, alias: %s", updatedProject.getName()));
        updatedProject.setId(id);
        return repository.saveAndFlush(updatedProject);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public RedcapProject getProject(@PathVariable Integer id) {
        traceAction(String.format("retrieve REDCap project, id: %d", id));
        RedcapProject project = repository.findOne(id);
        if (project != null) {
            return project;
        } else {
            throw new ResourceNotFoundException("Project with ID '" + id + "' not found on server");
        }

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProject(@PathVariable Integer id) {
        traceAction(String.format("delete REDCap project, id: %d", id));
        repository.delete(id);
    }
}
