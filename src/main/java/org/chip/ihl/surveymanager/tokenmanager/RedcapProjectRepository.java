package org.chip.ihl.surveymanager.tokenmanager;

import org.chip.ihl.surveymanager.redcap.RedcapProject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by sboykin on 4/29/2015.
 */
@Repository
public interface RedcapProjectRepository extends JpaRepository<RedcapProject, Integer> {
    public List<RedcapProject> findByUrl(String url);
    public RedcapProject findFirstByUrl(String url);
}
