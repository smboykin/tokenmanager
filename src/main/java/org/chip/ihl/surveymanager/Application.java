package org.chip.ihl.surveymanager;

import org.chip.ihl.surveymanager.config.MyscilhsProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.annotation.Order;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.security.Principal;

@PropertySources(value = {@PropertySource(value = "java:comp/env/myscilhsConfigFile", ignoreResourceNotFound = true)})
@ComponentScan
@EnableAutoConfiguration
@RestController
public class Application {

    /**
     * Simple controller to handle authentication
     * @param user
     * @return
     */
    @RequestMapping("/user")
    public Principal user(Principal user) {
        return user;
    }

    public static void main(String[] args) {
        // clean out any left over ActiveMQ data from previous run
        FileSystemUtils.deleteRecursively(new File("activemq-data"));

        SpringApplication.run(Application.class, args);
    }
}
