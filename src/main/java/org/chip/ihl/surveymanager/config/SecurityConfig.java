package org.chip.ihl.surveymanager.config;

import org.chip.ihl.surveymanager.AjaxAuthenticationFailureHandler;
import org.chip.ihl.surveymanager.AjaxAuthenticationSuccessHandler;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.ExceptionMappingAuthenticationFailureHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.security.web.util.matcher.RegexRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.WebUtils;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Spring Security configuration file
 * Created by sboykin on 4/23/2015.
 */
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Configuration
    public static class AdminWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            // Enforce security for all non-trigger pages and endpoints (admin pages and services)
            http
                    .authorizeRequests()
                        .antMatchers(HttpMethod.POST, "/auth").permitAll()
                        .antMatchers("/admin/**").hasRole("USER")
                    //.antMatchers("/index.html", "/partials/home.html", "/partials/login.html", "/").permitAll().anyRequest()
                    //.antMatchers("/login.html").permitAll()
                        .anyRequest().authenticated()
                    .and()
//                    .formLogin().defaultSuccessUrl("/admin").permitAll()
                .formLogin()
                    .loginPage("/login.html").loginProcessingUrl("/auth")
                    .usernameParameter("username").passwordParameter("password")
                    .successHandler(new AjaxAuthenticationSuccessHandler(new SavedRequestAwareAuthenticationSuccessHandler()))
                    .failureHandler(new AjaxAuthenticationFailureHandler(new ExceptionMappingAuthenticationFailureHandler()))
                    .permitAll()
                    .and()
//                    .logout().logoutUrl("/logout").logoutSuccessUrl("/").permitAll()
                    .logout().logoutUrl("/logout").logoutSuccessUrl("/login.html").permitAll()
                    .and()
                    .csrf().csrfTokenRepository(csrfTokenRepository())
                    .and()
                    .addFilterAfter(csrfHeaderFilter(), CsrfFilter.class);
        }

        private Filter csrfHeaderFilter() {
            return new OncePerRequestFilter() {
                @Override
                protected void doFilterInternal(HttpServletRequest request,
                                                HttpServletResponse response, FilterChain filterChain)
                        throws ServletException, IOException {
                    CsrfToken csrf = (CsrfToken) request.getAttribute(CsrfToken.class
                            .getName());
                    if (csrf != null) {
                        Cookie cookie = WebUtils.getCookie(request, "XSRF-TOKEN");
                        String token = csrf.getToken();
                        if (cookie == null || token != null
                                && !token.equals(cookie.getValue())) {
                            cookie = new Cookie("XSRF-TOKEN", token);
                            cookie.setPath("/");
                            response.addCookie(cookie);
                        }
                    }
                    filterChain.doFilter(request, response);
                }
            };
        }

        private CsrfTokenRepository csrfTokenRepository() {
            HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
            repository.setHeaderName("X-XSRF-TOKEN");
            return repository;
        }
    }

    @Configuration
    @Order(1)
    public static class WebServiceSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            // Allow unauthenticated web service requests for REDCap triggers
            http.antMatcher("/trigger/pull")
                    .csrf().disable()
                    .authorizeRequests()
                    .antMatchers("/trigger/pull").permitAll();
        }

    }
}
