/*
 * Copyright (c) 2014, Boston Children's Hospital. All Rights Reserved.
 *
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */

package org.chip.ihl.surveymanager.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * MySCILHS-specific config properties (the ones that aren't handled by spring boot
 * Created by sboykin on 12/9/2014.
 *
 */
@Component
@ConfigurationProperties(prefix = "myscilhs", ignoreUnknownFields = true)
public class MyscilhsProperties {

    public static class ActiveMQ {
        private int sendTimeout;
        private String queue;

        public int getSendTimeout() {
            return sendTimeout;
        }

        public void setSendTimeout(int sendTimeout) {
            this.sendTimeout = sendTimeout;
        }

        public String getQueue() {
            System.out.println("Setting the queue!!!");return queue;
        }

        public void setQueue(String queue) {
            this.queue = queue;
        }
    }

    private ActiveMQ activemq;

    public ActiveMQ getActivemq() {
        return activemq;
    }

    public void setActivemq(ActiveMQ activemq) {
        this.activemq = activemq;
    }


    @Override
    public String toString() {
        return "MyscilhsProperties{" +
                "activemq=" + activemq +
                '}';
    }
}
