/*
 * Copyright (c) 2014, Boston Children's Hospital. All Rights Reserved.
 *
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */

package org.chip.ihl.surveymanager.redcap;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * Holds REDCap project metadata (including token info)
 * Created by sboykin on 12/26/2014.
 */
@Entity
public class RedcapProject {
    @Id
    @GeneratedValue
    private Integer id;
    @NotEmpty(message = "{validation.notEmpty.message}")
    @Column(nullable = false)
    private String name;
    @ElementCollection
    private Set<String> forms;
    @Column(unique = true)
    @NotEmpty(message = "{validation.notEmpty.message}")
    @Pattern(regexp = "(http|https)://.+", message = "{validation.url.message}")
    private String url;
    @Column
    private String apiToken;

    public RedcapProject() {
        forms = new HashSet<>();
    }

    public RedcapProject(String url) {
        this();
        this.url = url;
    }

    public RedcapProject(String name, String url, String apiToken, Set<String> forms) {
        this.name = name;
        this.url = url;
        this.apiToken = apiToken;
        this.forms = forms;
    }

    public RedcapProject(String name, String url, String apiToken) {
        this();
        this.name = name;
        this.url = url;
        this.apiToken = apiToken;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public Set<String> getForms() { return forms; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RedcapProject that = (RedcapProject) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (url != null ? !url.equals(that.url) : that.url != null) return false;
        return !(apiToken != null ? !apiToken.equals(that.apiToken) : that.apiToken != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (url != null ? url.hashCode() : 0);
        return result;
    }
}
