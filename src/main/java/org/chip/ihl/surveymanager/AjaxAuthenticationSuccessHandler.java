package org.chip.ihl.surveymanager;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by sboykin on 5/11/2015.
 * Authentication success handler for AJAX calls
 */
public class AjaxAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    private AuthenticationSuccessHandler defaultHandler;

    public AjaxAuthenticationSuccessHandler(AuthenticationSuccessHandler defaultHandler) {
        this.defaultHandler = defaultHandler;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        if ("true".equals(httpServletRequest.getHeader("X-Login-Ajax-call"))) {
            httpServletResponse.setStatus(HttpServletResponse.SC_OK);
        } else {
            defaultHandler.onAuthenticationSuccess(httpServletRequest, httpServletResponse, authentication);
        }
    }
}
