package org.chip.ihl.surveymanager.controller;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for loading and saving wrapper properties
 * Created by sboykin on 5/1/2015.
 * TODO Copy from old surveymanager, eliminating wrapperconfiguration and using propertiesconfiguration directly
 */
@RestController
@RequestMapping("/admin/properties")
public class ManagerPropertiesController {
//    @Autowired
//    PropertiesConfiguration appProperties;

}
