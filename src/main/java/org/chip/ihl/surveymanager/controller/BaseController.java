package org.chip.ihl.surveymanager.controller;

import org.chip.ihl.surveymanager.exception.QueueException;
import org.chip.ihl.surveymanager.exception.ResourceNotFoundException;
import org.chip.ihl.surveymanager.exception.UnacceptableException;
import org.chip.ihl.surveymanager.exception.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.trace.TraceRepository;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sboykin on 5/6/2015. (With assist/inspiration from https://github.com/kms-technology/)
 *
 */
public class BaseController {
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    private TraceRepository traceRepository;

    public BaseController(TraceRepository repository) {
        this.traceRepository = repository;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, String> handleValidationError(MethodArgumentNotValidException ex) {
        Map<String, String> fieldErrorMap = new HashMap<>();
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
        for (FieldError fieldError: fieldErrors) {
            fieldErrorMap.put(fieldError.getField(), fieldError.getDefaultMessage());
            logger.debug("fielderror: " + fieldError.toString());
        }
        return fieldErrorMap;
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String handleUnexpectedException(Exception ex) {
        logger.error("Error during process request", ex);
        return ex.getMessage();
    }

    /***** Exception Handlers ****/
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Missing or invalid arguments")
    @ExceptionHandler(IllegalArgumentException.class)
    public String badArguments(Exception ex) {
        logger.error("Error in client request", ex);
        return ex.getMessage();
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Records not found")
    @ExceptionHandler(ResourceNotFoundException.class)
    public void notFound(Exception ex) {
        logger.warn("Request not found", ex);
    }

    @ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "Unauthorized")
    @ExceptionHandler(UnauthorizedException.class)
    public void unAuthorized(Exception ex) {
        logger.error("Request not authorized", ex);
    }

    @ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE, reason = "Data not acceptable")
    @ExceptionHandler(UnacceptableException.class)
    public String unacceptable(Exception ex) {
        logger.error("Request data not acceptable", ex);
        return ex.getMessage();
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Message queue exception")
    @ExceptionHandler(QueueException.class)
    public String unpublished(Exception ex) {
        logger.error("Requested record not published", ex);
        return ex.getMessage();
    }

    protected void traceAction(String action) {
        String user = "anonymous";
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            user = authentication.getName();
        }

        Map<String, Object> trace = new LinkedHashMap<>();
        trace.put("user", user);
        trace.put("action", action);

        traceRepository.add(trace);
    }
}
