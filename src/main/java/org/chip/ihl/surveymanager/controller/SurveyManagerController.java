/*
 * Copyright (c) 2014, Boston Children's Hospital. All Rights Reserved.
 *
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */

package org.chip.ihl.surveymanager.controller;

import org.chip.ihl.surveymanager.config.MyscilhsProperties;
import org.chip.ihl.surveymanager.exception.QueueException;
import org.chip.ihl.surveymanager.exception.ResourceNotFoundException;
import org.chip.ihl.surveymanager.exception.UnacceptableException;
import org.chip.ihl.surveymanager.exception.UnauthorizedException;
import org.chip.ihl.surveymanager.jms.MessageProducerBean;
import org.chip.ihl.surveymanager.jms.SurveyMessage;
import org.chip.ihl.surveymanager.redcap.EAVSurveyRecord;
import org.chip.ihl.surveymanager.redcap.RedcapResult;
import org.chip.ihl.surveymanager.service.RedcapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.trace.TraceRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

/**
 * Web org.chip.ihl.surveymanager.service endpoints for for REDCap interaction
 * Created by sboykin on 11/23/2014.
 *
 */
@RestController
public class SurveyManagerController extends BaseController {
    private static final String EAV_RECORD_TYPE = "eav";
    public static final String TRIGGER_BASE_REQUEST_URI = "/trigger";

    private RedcapService redcapService;
    private MessageProducerBean producerBean;
    private MyscilhsProperties myscilhsProperties;

    @Autowired
    public SurveyManagerController(RedcapService redcapService, MessageProducerBean producerBean, MyscilhsProperties properties, TraceRepository traceRepository) {
        super(traceRepository);
        this.redcapService = redcapService;
        this.producerBean = producerBean;
        this.myscilhsProperties = properties;
    }

//    public void setRedcapService(RedcapService service) {
//        this.redcapService = service;
//    }
//    public void setProducerBean(MessageProducerBean producerBean) {
//        this.producerBean = producerBean;
//    }

    /**
     * Retrieves REDCap records for a particular subject and pushes to message queue
     * @param recordId  The survey subject ID (relative to project)
     * @param recordType    The record format type e.g. 'eav' (default) or 'flat'
     //* @param projectToken  The API token for the project to pull from
     * @param eventName    (optional) The event name to pull from, if longitudnal study
     * @param surveyForm    (optional) The survey form to pull form
     */
    @RequestMapping(value = TRIGGER_BASE_REQUEST_URI + "/pull", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE})
    @ResponseStatus(value = HttpStatus.OK)
    public void pullRedcapRecords(
            @RequestParam(value="record", required = false) String recordId,
            @RequestParam(value = "recordType", defaultValue = EAV_RECORD_TYPE) String recordType,
            //@RequestParam(value = "token", required = true) String projectToken,
            @RequestParam(value = "redcap_event_name", required = false) String eventName,
            @RequestParam(value = "redcap_url", required = true) String redcapBaseUrl,
            @RequestParam(value = "project_url", required = true) String redcapProjectUrl,
            @RequestParam(value = "instrument", required = false) String surveyForm) {

        traceAction(String.format("pull REDCap record, id: %s, project: %s", recordId, redcapProjectUrl));
        RedcapResult redcapResult = redcapService.pullRecordRequest(redcapBaseUrl, redcapProjectUrl, recordType, recordId, surveyForm, eventName);
        switch (redcapResult.getStatus()) {
            case OK:
                ArrayList<EAVSurveyRecord> records = redcapResult.getRecords();
                if (records == null || records.isEmpty()) {
                    logger.warn("REDCap returned no records to push for ID: " + recordId);
                   //throw new ResourceNotFoundException("REDCap returned no records to push.");
                }
                else {
                    String queue = myscilhsProperties.getActivemq().getQueue();
                    try {
                        producerBean.sendMessage(queue, new SurveyMessage(records));
                        logger.info("Successfully pulled REDCap records and pushed to message queue: " + queue);
                    } catch (Exception me) {
                        logger.error("Problem sending pulled REDCap records to survey results queue", me);
                        throw new QueueException("Problem sending records to message queue: " + queue, me);
                    }
                }
                break;
            case NOT_FOUND:
                throw new ResourceNotFoundException("Not found");
            case UNAUTHORIZED:
                throw new UnauthorizedException(redcapResult.getRedcapError().getError());
            case FORBIDDEN:
                throw new UnauthorizedException(redcapResult.getRedcapError().getError());
            case NOT_ACCEPTABLE:
                throw new UnacceptableException(redcapResult.getRedcapError().getError());
            default:
                throw new RuntimeException(redcapResult.getRedcapError().getError());
        }
    }

}
