package org.chip.ihl.surveymanager.tokenmanager.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.chip.ihl.surveymanager.config.TestConfig;
import org.chip.ihl.surveymanager.redcap.RedcapProject;
import org.chip.ihl.surveymanager.tokenmanager.RedcapProjectRepository;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.trace.TraceRepository;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.chip.ihl.surveymanager.redcap.RedcapData.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.testng.Assert.assertEquals;
/**
 * Created by sboykin on 5/13/2015.
 */
@SpringApplicationConfiguration(classes = TestConfig.class)
@WebAppConfiguration
@ActiveProfiles("unit-test")
public class RedcapProjectControllerTest extends AbstractTestNGSpringContextTests {
    protected MockMvc mockMvc;
    private final String PROJECTS_URL = "/redprojects";

    @Mock
    private RedcapProjectRepository projectRepositoryMock;
    @Autowired
    private TraceRepository traceRepository;
    @InjectMocks
    private RedcapProjectController controllerUnderTest;
    @Autowired
    private ObjectMapper objectMapper;

    @BeforeClass
    public void initGlobal() {
        MockitoAnnotations.initMocks(this);
    }

    @BeforeMethod
    public void setup() {
        Mockito.reset(projectRepositoryMock);
        controllerUnderTest = new RedcapProjectController(projectRepositoryMock, traceRepository);
        mockMvc = MockMvcBuilders.standaloneSetup(controllerUnderTest).build();
    }


    @Test
    public void findProjects_shouldReturnProjects() throws Exception {
        List<RedcapProject> projects = getRedcapProjects(3);
        when(projectRepositoryMock.findAll()).thenReturn(projects);

        MvcResult result = mockMvc.perform(get(PROJECTS_URL)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andReturn();
        verify(projectRepositoryMock).findAll();
        String returnedProjectsAsJson = result.getResponse().getContentAsString();
        assertEquals(returnedProjectsAsJson, objectMapper.writeValueAsString(projects), "Returned projects don't match expected.");
    }

    @Test
    public void findProjects_shouldThrowServerErrorWhenRepoThrowsException() throws Exception {
        List<RedcapProject> projects = getRedcapProjects(3);
        when(projectRepositoryMock.findAll()).thenThrow(Exception.class);

        MvcResult result = mockMvc.perform(get(PROJECTS_URL)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isInternalServerError())
                .andReturn();
        verify(projectRepositoryMock).findAll();
    }

    @Test
    public void addProjectShouldReturnCreated() throws Exception {
        RedcapProject projectToAdd = generateRedcapProject(1, TEST_PROJECT_URL);
        when(projectRepositoryMock.saveAndFlush((RedcapProject) any())).thenReturn(projectToAdd);
        MvcResult result = mockMvc.perform(post(PROJECTS_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(projectToAdd))
        )
                .andExpect(status().isCreated())
                .andReturn();
        verify(projectRepositoryMock).saveAndFlush((RedcapProject) any());
        String returnedProjectsAsJson = result.getResponse().getContentAsString();
        assertEquals(returnedProjectsAsJson, objectMapper.writeValueAsString(projectToAdd), "Created project doesn't match expected.");

    }

    @Test
    public void addProject_shouldReturnBadRequestForObjectWithInvalidUrl() throws Exception {
        RedcapProject project = generateRedcapProject(1, "AnInvalidUrl");
        mockMvc.perform(post(PROJECTS_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(project))
        )
                .andExpect(status().isBadRequest());
        verify(projectRepositoryMock, never()).saveAndFlush((RedcapProject) any());
    }
    @Test
    public void addProject_shouldReturnBadRequestForObjectWithNullAlias() throws Exception {
        RedcapProject project = generateRedcapProject(1, TEST_PROJECT_URL);
        project.setName(null);
        mockMvc.perform(post(PROJECTS_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(project))
        )
                .andExpect(status().isBadRequest());
        verify(projectRepositoryMock, never()).saveAndFlush((RedcapProject) any());
    }

    @Test
    public void updateProject_shouldReturnUpdated() throws Exception {
        RedcapProject projectToUpdate = generateRedcapProject(1, TEST_PROJECT_URL);
        when(projectRepositoryMock.saveAndFlush(eq(projectToUpdate))).thenReturn(projectToUpdate);
        String projectUrl = PROJECTS_URL + "/1";
        MvcResult result = mockMvc.perform(put(projectUrl)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(projectToUpdate))
        )
                .andExpect(status().isAccepted())
                .andReturn();
        String returnedProjectsAsJson = result.getResponse().getContentAsString();
        assertEquals(returnedProjectsAsJson, objectMapper.writeValueAsString(projectToUpdate), "Updated project doesn't match expected.");
    }

    @Test
    public void updateProject_shouldReturnBadRequestForObjectWithInvalidUrl() throws Exception {
        RedcapProject project = generateRedcapProject(1, "AnInvalidUrl");
        String projectUrl = PROJECTS_URL + "/1";
        mockMvc.perform(put(projectUrl)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(project))
        )
                .andExpect(status().isBadRequest());
        verify(projectRepositoryMock, never()).saveAndFlush((RedcapProject) any());
    }
    @Test
    public void updateProject_shouldReturnBadRequestForObjectWithNullAlias() throws Exception {
        RedcapProject project = generateRedcapProject(1, TEST_PROJECT_URL);
        project.setName(null);
        String projectUrl = PROJECTS_URL + "/1";
        mockMvc.perform(put(projectUrl)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(project))
        )
                .andExpect(status().isBadRequest());
        verify(projectRepositoryMock, never()).saveAndFlush((RedcapProject) any());
    }

    @Test
    public void getProject_shouldReturnProject() throws Exception {
        RedcapProject project = generateRedcapProject(1, TEST_PROJECT_URL);
        when(projectRepositoryMock.findOne(1)).thenReturn(project);
        String projectUrl = PROJECTS_URL + "/1";
        MvcResult result = mockMvc.perform(get(projectUrl)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andReturn();
        verify(projectRepositoryMock).findOne(1);
        String returnedProjectAsJson = result.getResponse().getContentAsString();
        assertEquals(returnedProjectAsJson, objectMapper.writeValueAsString(project), "Returned project don't match expected.");
    }

    @Test
    public void getProject_shouldReturn404() throws Exception {
        when(projectRepositoryMock.findOne(1)).thenReturn(null);
        String projectUrl = PROJECTS_URL + "/1";
        mockMvc.perform(get(projectUrl)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isNotFound());
        verify(projectRepositoryMock).findOne(1);
    }


    @Test
    public void deleteProject_shouldReturnNoContent() throws Exception {
        String projectUrl = PROJECTS_URL + "/1";
        mockMvc.perform(delete(projectUrl)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isNoContent());
        verify(projectRepositoryMock).delete(1);
    }
}