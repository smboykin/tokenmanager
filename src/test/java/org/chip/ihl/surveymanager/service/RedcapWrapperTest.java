/*
 * Copyright (c) 2014, Boston Children's Hospital. All Rights Reserved.
 *
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */

package org.chip.ihl.surveymanager.service;

import org.chip.ihl.surveymanager.Application;
import org.chip.ihl.surveymanager.config.AppConfig;
import org.chip.ihl.surveymanager.config.MyscilhsProperties;
import org.chip.ihl.surveymanager.exception.ResourceNotFoundException;
import org.chip.ihl.surveymanager.redcap.EAVSurveyRecord;
import org.chip.ihl.surveymanager.redcap.RedcapResult;
import org.chip.ihl.surveymanager.tokenmanager.RedcapProjectRepository;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.*;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.web.client.RestTemplate;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.*;

import static org.chip.ihl.surveymanager.redcap.RedcapData.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

/**
 * Tests REDCap wrapper functionality
 */
@Test
@ContextConfiguration(classes = AppConfig.class)
public class RedcapWrapperTest extends AbstractTestNGSpringContextTests {
    private final Logger logger = LoggerFactory.getLogger(RedcapWrapperTest.class);

    private Properties properties;

    private RedcapWrapper validWrapper;

    @Mock
    private RestTemplate mockRestTemplate;

    @Mock
    private RedcapProjectRepository mockRepository;

    @Mock
    private RestTemplate restTemplate;

    @BeforeTest(alwaysRun = true)
    public void setup() throws Exception {
        // setup mocks
        MockitoAnnotations.initMocks(this);

        validWrapper = getWrapperFromParams();

//        // pull REDCap properties
//        properties = new Properties();
//        InputStream inputStream;
//        try {
//            inputStream = RedcapWrapperTest.class.getClassLoader().getResourceAsStream("application.properties");
//            if (inputStream == null) {
//                throw new IOException("Can't load test properties file");
//            }
//            properties.load(inputStream);
//
////            Reporter.log(String.format("Configuration components:\nREDCap Base URL: %s; Token: %s\n",
////                    testBaseUrl, testApiToken));
//
//            validWrapper = getWrapperFromParams();
//        } catch (IOException ie) {
//            throw new RuntimeException("Can't load test properties file", ie);
//        }
//        // default behavior for mock repo
    }
    // pullRecordRequest tests
    @Test(groups = "static")
    public void whenValidParametersPullRecordRequestReturnsOk() throws Exception {
        when(mockRepository.findFirstByUrl(anyString())).thenReturn(goodRedcapProjectWithForms());
        when(mockRestTemplate.exchange(anyString(),eq(HttpMethod.POST),any(HttpEntity.class), eq(String.class))).thenReturn(goodResponse());
        RedcapResult result = validWrapper.pullRecordRequest(TEST_BASE_URL, TEST_PROJECT_URL, EAV_RECORD_TYPE, TEST_RECORD_ID, APPROVED_FORM1, TEST_EVENT_NAME);
        Assert.assertEquals(HttpStatus.OK, result.getStatus());
        Assert.assertTrue(result.getRedcapError() == null || result.getRedcapError().getError() == null || result.getRedcapError().getError().isEmpty());
        List<EAVSurveyRecord> actuals = result.getRecords();
        List<EAVSurveyRecord> expecteds = Arrays.asList(sampleRedcapRecords());
        Assert.assertEquals(actuals, expecteds);
    }

    @Test(groups = "static", expectedExceptions = IllegalArgumentException.class)
    public void whenMissingRecordTypePullRecordRequestThrowsException() {
        RedcapResult result = validWrapper.pullRecordRequest(TEST_BASE_URL, TEST_PROJECT_URL, null, TEST_RECORD_ID, APPROVED_FORM2, TEST_EVENT_NAME);
    }

    @Test(groups = "static", expectedExceptions = IllegalArgumentException.class)
    public void whenInvalidRecordTypePullRecordRequestThrowsException() {
        RedcapResult result = validWrapper.pullRecordRequest(TEST_BASE_URL, TEST_PROJECT_URL, "myinvalidrecordtype", TEST_RECORD_ID, APPROVED_FORM2, TEST_EVENT_NAME);
    }


    @Test(groups = "static", expectedExceptions = IllegalArgumentException.class)
    public void whenMissingProjectUrlPullRecordRequestThrowsException() throws Exception {
        String projectUrl = null;
        RedcapWrapper wrapper = getWrapperFromParams();
        RedcapResult result = wrapper.pullRecordRequest(TEST_BASE_URL, projectUrl, EAV_RECORD_TYPE, TEST_RECORD_ID, APPROVED_FORM2, TEST_EVENT_NAME);
    }


    @Test(groups = "static", expectedExceptions = ResourceNotFoundException.class)
    public void whenProjectUrlHasNoStoredTokenPullRecordRequestThrowsException() {
        RedcapWrapper wrapper = getWrapperFromParams();
        when(mockRepository.findFirstByUrl(anyString())).thenReturn(null);
        RedcapResult result = wrapper.pullRecordRequest(TEST_BASE_URL, TEST_PROJECT_URL, EAV_RECORD_TYPE, TEST_RECORD_ID, APPROVED_FORM2, TEST_EVENT_NAME);
    }

    @Test(groups = "static")
    public void whenInvalidTokenPullRecordRequestReturns403() throws Exception {
        when(mockRepository.findFirstByUrl(anyString())).thenReturn(goodRedcapProjectWithForms());
        when(mockRestTemplate.exchange(anyString(),eq(HttpMethod.POST),any(HttpEntity.class), eq(String.class))).thenReturn(unAuthorizedResponse());
        RedcapWrapper wrapper = getWrapperFromParams();
        RedcapResult result = wrapper.pullRecordRequest(TEST_BASE_URL, TEST_PROJECT_URL, EAV_RECORD_TYPE, TEST_RECORD_ID, APPROVED_FORM1, TEST_EVENT_NAME);
        Assert.assertEquals(HttpStatus.UNAUTHORIZED, result.getStatus());
    }

    @Test(groups = "static")
    public void whenInvalidRedcapApiUrlPullRecordRequestReturns404() {
        when(mockRepository.findFirstByUrl(anyString())).thenReturn(goodRedcapProjectWithForms());
        when(mockRestTemplate.exchange(anyString(),eq(HttpMethod.POST),any(HttpEntity.class), eq(String.class))).thenReturn(notFoundResponse());
        RedcapWrapper wrapper = getWrapperFromParams();
        // Test bad API uri
        RedcapResult result = wrapper.pullRecordRequest(TEST_BASE_URL, TEST_PROJECT_URL, EAV_RECORD_TYPE, TEST_RECORD_ID, APPROVED_FORM2, TEST_EVENT_NAME);
        Assert.assertEquals(HttpStatus.NOT_FOUND, result.getStatus());
    }

    // TODO : check
//    @Test(groups = "static", expectedExceptions = IllegalArgumentException.class)
//    public void whenPrivateFormRequestedPullRecordRequestReturnsException() {
//        when(mockRepository.findFirstByUrl(anyString())).thenReturn(goodRedcapProject());
//        // This only works for one form in list
//        String surveyForm = myscilhsProps.getRedcap().getPrivateForms().get(0);
//        List<String> privateForms = new ArrayList<>(1);
//        privateForms.add(surveyForm);
//        RedcapWrapper wrapper = getWrapperFromParams();
//        RedcapResult result = wrapper.pullRecordRequest(TEST_BASE_URL, TEST_PROJECT_URL, EAV_RECORD_TYPE, TEST_RECORD_ID, surveyForm, TEST_EVENT_NAME);
//    }

    @Test(groups = "static", expectedExceptions = IllegalArgumentException.class)
    public void whenFormIsNotApprovedPullRecordRequestReturnsException() {
        when(mockRepository.findFirstByUrl(anyString())).thenReturn(goodRedcapProjectWithForms());
        RedcapWrapper wrapper = getWrapperFromParams();
        RedcapResult result = wrapper.pullRecordRequest(TEST_BASE_URL, TEST_PROJECT_URL, EAV_RECORD_TYPE, TEST_RECORD_ID, UNAPPROVED_FORM, TEST_EVENT_NAME);
    }

    @Test(groups = "static", expectedExceptions = IllegalArgumentException.class)
    public void whenMissingRedcapApiUrlPullRecordRequestReturns404() {
        RedcapWrapper wrapper = getWrapperFromParams();
        // Test bad API uri
        RedcapResult result = wrapper.pullRecordRequest(null, TEST_PROJECT_URL, EAV_RECORD_TYPE, TEST_RECORD_ID, APPROVED_FORM2, TEST_EVENT_NAME);
    }

    // HELPERS
//    private RedcapWrapper getWrapperFromParams(List<String> privateForms) {
//        MyscilhsProperties mprops = new MyscilhsProperties();
//        //mprops.setRedcapApiToken(apiToken);
//        if (privateForms != null) {
//            mprops.getRedcap().getPrivateForms().addAll(privateForms);
//        }
//        return new RedcapWrapper(mprops, mockRepository, mockRestTemplate);
//    }

    private RedcapWrapper getWrapperFromParams() {
        return new RedcapWrapper(mockRepository, mockRestTemplate);
    }

    private HttpEntity<EAVSurveyRecord> setupTestHttpEntity() {
        List<MediaType> acceptTypes = new ArrayList<>(1);
        acceptTypes.add(MediaType.APPLICATION_JSON);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(acceptTypes);

        return new HttpEntity<>(headers);
    }

    private ResponseEntity<String> goodResponse() {
        return new ResponseEntity<>(sampleRedcapRecordsAsJson(), HttpStatus.OK);
    }

    private ResponseEntity<String> emptyResponse() {
        return new ResponseEntity<>("[]", HttpStatus.OK);
    }
    private ResponseEntity<String> unAuthorizedResponse() {
        return new ResponseEntity<>("[]", HttpStatus.UNAUTHORIZED);
    }

    private ResponseEntity<String> notFoundResponse() {
        return new ResponseEntity<>("[]", HttpStatus.NOT_FOUND);
    }

}