package org.chip.ihl.surveymanager.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Created by sboykin on 5/13/2015.
 * Configuration for tests
 */
@Profile("unit-test")
@Configuration
@ComponentScan(basePackages = "org.chip.ihl.surveymanager")
@EnableAutoConfiguration
public class TestConfig {
}
