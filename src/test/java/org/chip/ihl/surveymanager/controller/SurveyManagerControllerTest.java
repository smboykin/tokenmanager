package org.chip.ihl.surveymanager.controller;

import org.chip.ihl.surveymanager.Application;
import org.chip.ihl.surveymanager.config.MyscilhsProperties;
import org.chip.ihl.surveymanager.config.TestConfig;
import org.chip.ihl.surveymanager.exception.ResourceNotFoundException;
import org.chip.ihl.surveymanager.jms.MessageProducerBean;
import org.chip.ihl.surveymanager.jms.SurveyMessage;
import org.chip.ihl.surveymanager.redcap.EAVSurveyRecord;
import org.chip.ihl.surveymanager.redcap.RedcapResult;
import org.chip.ihl.surveymanager.service.RedcapService;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.trace.TraceRepository;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.chip.ihl.surveymanager.redcap.RedcapData.*;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.testng.Assert.*;

/**
 * Created by sboykin on 5/12/2015.
 * Unit Tests the SurveyManagerController for expected success and error outcomes
 */
@SpringApplicationConfiguration(classes = TestConfig.class)
@WebAppConfiguration
@ActiveProfiles("unit-test")
public class SurveyManagerControllerTest extends AbstractTestNGSpringContextTests {
    protected MockMvc mockMvc;
    private final String REDCAP_PULL_URI = "/trigger/pull";

    @Mock
    RedcapService redcapServiceMock;
    @Mock
    MessageProducerBean producerBeanMock;
    @Autowired
    private MyscilhsProperties myscilhsProperties;
    @Autowired
    TraceRepository traceRepository;

    //@Autowired
    //protected WebApplicationContext webApplicationContext;
    @InjectMocks
    private SurveyManagerController controllerUnderTest;


    private void resetMocks() {
        Mockito.reset(redcapServiceMock, producerBeanMock);
    }

    @BeforeClass
    public void initGlobal() {
        MockitoAnnotations.initMocks(this);
    }

    @BeforeMethod
    public void setup() {
        resetMocks();
        controllerUnderTest = new SurveyManagerController(redcapServiceMock, producerBeanMock, myscilhsProperties, traceRepository);
        mockMvc = MockMvcBuilders.standaloneSetup(controllerUnderTest).build();
    }


    @Test
    public void pullRedcapRecords_shouldReturnOkForSingleRecordId() throws Exception {
        RedcapResult redcapResult = redcapResultForSingleId(TEST_RECORD_ID);
        when(redcapServiceMock.pullRecordRequest(anyString(), anyString(), anyString(), eq(TEST_RECORD_ID), anyString(), eq(TEST_EVENT_NAME))).thenReturn(redcapResult);

        mockMvc.perform(post(REDCAP_PULL_URI)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .accept(MediaType.APPLICATION_JSON)
                        .param("record", TEST_RECORD_ID)
                        .param("recordType", EAV_RECORD_TYPE)
                        .param("redcap_event_name", TEST_EVENT_NAME)
                        .param("redcap_url", TEST_BASE_URL)
                        .param("project_url", TEST_PROJECT_URL)
                        .param("instrument", APPROVED_FORM1)
        )
                .andExpect(status().isOk());
        ArgumentCaptor<SurveyMessage> messageArg = ArgumentCaptor.forClass(SurveyMessage.class);
        verify(producerBeanMock).sendMessage(eq(myscilhsProperties.getActivemq().getQueue()), messageArg.capture());
        assertEquals(messageArg.getValue().getRecords(), redcapResult.getRecords(), "Records do not match");

    }

    @Test
    public void pullRedcapRecords_shouldReturnNotFoundWhenRedcapResultNotFound() throws Exception {
        when(redcapServiceMock.pullRecordRequest(anyString(), anyString(), anyString(), eq(TEST_RECORD_ID), anyString(), eq(TEST_EVENT_NAME))).thenThrow(ResourceNotFoundException.class);

        mockMvc.perform(post(REDCAP_PULL_URI)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .accept(MediaType.APPLICATION_JSON)
                        .param("record", TEST_RECORD_ID)
                        .param("recordType", EAV_RECORD_TYPE)
                        .param("redcap_event_name", TEST_EVENT_NAME)
                        .param("redcap_url", TEST_BASE_URL)
                        .param("project_url", TEST_PROJECT_URL)
                        .param("instrument", APPROVED_FORM1)
        )
                .andExpect(status().isNotFound());
        // and no message should be sent
        verify(producerBeanMock, never()).sendMessage(anyString(), (SurveyMessage) anyObject());
    }

    @Test
    public void pullRedcapRecords_shouldReturnOkWhenRedcapResultRecordsEmpty() throws Exception {
        RedcapResult redcapResult = emptyRedcapResultWithStatus(HttpStatus.OK);
        when(redcapServiceMock.pullRecordRequest(anyString(), anyString(), anyString(), eq(TEST_RECORD_ID), anyString(), eq(TEST_EVENT_NAME))).thenReturn(redcapResult);

        mockMvc.perform(post(REDCAP_PULL_URI)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .accept(MediaType.APPLICATION_JSON)
                        .param("record", TEST_RECORD_ID)
                        .param("recordType", EAV_RECORD_TYPE)
                        .param("redcap_event_name", TEST_EVENT_NAME)
                        .param("redcap_url", TEST_BASE_URL)
                        .param("project_url", TEST_PROJECT_URL)
                        .param("instrument", APPROVED_FORM1)
        )
                .andExpect(status().isOk());
        // and no message should be sent
        verify(producerBeanMock, never()).sendMessage(anyString(), (SurveyMessage) anyObject());
    }

    @Test
    public void pullRedcapRecords_shouldReturnBadRequestWhenWrapperThrowsIllegalArgumentException() throws Exception {
        when(redcapServiceMock.pullRecordRequest(anyString(), anyString(), anyString(), eq(TEST_RECORD_ID), anyString(), eq(TEST_EVENT_NAME))).thenThrow(IllegalArgumentException.class);
        mockMvc.perform(post(REDCAP_PULL_URI)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .accept(MediaType.APPLICATION_JSON)
                        .param("record", TEST_RECORD_ID)
                        .param("recordType", EAV_RECORD_TYPE)
                        .param("redcap_event_name", TEST_EVENT_NAME)
                        .param("redcap_url", TEST_BASE_URL)
                        .param("project_url", TEST_PROJECT_URL)
                        .param("instrument", APPROVED_FORM1)
        )
                .andExpect(status().isBadRequest());
        // and no message should be sent
        verify(producerBeanMock, never()).sendMessage(anyString(), (SurveyMessage) anyObject());
    }

    @Test
    public void pullRedcapRecords_shouldReturnUnauthorizedWhenWrapperReturnsUnauthorized() throws Exception {
        RedcapResult redcapResult = emptyRedcapResultWithStatus(HttpStatus.UNAUTHORIZED);
        when(redcapServiceMock.pullRecordRequest(anyString(), anyString(), anyString(), eq(TEST_RECORD_ID), anyString(), eq(TEST_EVENT_NAME))).thenReturn(redcapResult);
        mockMvc.perform(post(REDCAP_PULL_URI)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .accept(MediaType.APPLICATION_JSON)
                        .param("record", TEST_RECORD_ID)
                        .param("recordType", EAV_RECORD_TYPE)
                        .param("redcap_event_name", TEST_EVENT_NAME)
                        .param("redcap_url", TEST_BASE_URL)
                        .param("project_url", TEST_PROJECT_URL)
                        .param("instrument", APPROVED_FORM1)
        )
                .andExpect(status().isUnauthorized());
        // and no message should be sent
        verify(producerBeanMock, never()).sendMessage(anyString(), (SurveyMessage) anyObject());
    }
    @Test
    public void pullRedcapRecords_shouldReturnNotAcceptableWhenWrapperReturnsNotAcceptable() throws Exception {
        RedcapResult redcapResult = emptyRedcapResultWithStatus(HttpStatus.NOT_ACCEPTABLE);
        when(redcapServiceMock.pullRecordRequest(anyString(), anyString(), anyString(), eq(TEST_RECORD_ID), anyString(), eq(TEST_EVENT_NAME))).thenReturn(redcapResult);
        mockMvc.perform(post(REDCAP_PULL_URI)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .accept(MediaType.APPLICATION_JSON)
                        .param("record", TEST_RECORD_ID)
                        .param("recordType", EAV_RECORD_TYPE)
                        .param("redcap_event_name", TEST_EVENT_NAME)
                        .param("redcap_url", TEST_BASE_URL)
                        .param("project_url", TEST_PROJECT_URL)
                        .param("instrument", APPROVED_FORM1)
        )
                .andExpect(status().isNotAcceptable());
        // and no message should be sent
        verify(producerBeanMock, never()).sendMessage(anyString(), (SurveyMessage) anyObject());
    }
    @Test
    public void pullRedcapRecords_shouldReturnServerErrorWhenWrapperReturnsServerError() throws Exception {
        RedcapResult redcapResult = emptyRedcapResultWithStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        when(redcapServiceMock.pullRecordRequest(anyString(), anyString(), anyString(), eq(TEST_RECORD_ID), anyString(), eq(TEST_EVENT_NAME))).thenReturn(redcapResult);
        mockMvc.perform(post(REDCAP_PULL_URI)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .accept(MediaType.APPLICATION_JSON)
                        .param("record", TEST_RECORD_ID)
                        .param("recordType", EAV_RECORD_TYPE)
                        .param("redcap_event_name", TEST_EVENT_NAME)
                        .param("redcap_url", TEST_BASE_URL)
                        .param("project_url", TEST_PROJECT_URL)
                        .param("instrument", APPROVED_FORM1)
        )
                .andExpect(status().isInternalServerError());
        // and no message should be sent
        verify(producerBeanMock, never()).sendMessage(anyString(), (SurveyMessage) anyObject());
    }
}