/*
 * Copyright (c) 2014, Boston Children's Hospital. All Rights Reserved.
 *
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */

package org.chip.ihl.surveymanager.redcap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Common static methods to create test data
 * Created by sboykin on 11/25/2014.
 *
 */
public class RedcapData {
    public static EAVSurveyRecord[] sampleRedcapRecords() {
        EAVSurveyRecord[] records = new EAVSurveyRecord[3];
        records[0] = new EAVSurveyRecord("record_1", "my_field_name_1", "my_event_name_1", "my_value_1");
        records[1] = new EAVSurveyRecord("record_2", "my_field_name_2", "my_event_name_2", "my_value_2");
        records[2] = new EAVSurveyRecord("record_3", "my_field_name_3", "my_event_name_3", "my_value_3");

        return records;
    }

    public static String sampleRedcapRecordsAsJson() throws RuntimeException {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(sampleRedcapRecords());
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Cannot convert records to JSON", e);
        }
    }

    public static EAVSurveyRecord[] emptyRedcapRecords() {
        return new EAVSurveyRecord[0];
    }

    public static RedcapProject goodRedcapProject(String[] formsToInclude) {
        RedcapProject project = new RedcapProject("project_name", TEST_PROJECT_URL, FAKE_API_TOKEN);
        if (formsToInclude != null) {
            project.getForms().addAll(Arrays.asList(formsToInclude));
        }
        return project;
    }

    public static RedcapProject goodRedcapProjectWithForms() {
        return goodRedcapProject(APPROVED_FORMS);
    }

    public static final String EAV_RECORD_TYPE = "eav";
    public static final String INVALID_RECORD_TYPE = "eav";
    public static final String TEST_EVENT_NAME = "event_1_arm_1";
    public static final String TEST_RECORD_ID = "1";
    public static final String TEST_PROJECT_URL = "http://testredcapurl";
    public static final String TEST_PROJECT_ALIAS = "Test REDCap Project";
    public static final String TEST_BASE_URL = "http://testredcapurl?project_id=1";
    public static final String FAKE_API_TOKEN = "1234567ABCDEFGH";
    public static final String APPROVED_FORM1 = "approvedForm1";
    public static final String APPROVED_FORM2 = "approvedForm2";
    public static final String UNAPPROVED_FORM = "unapprovedForm1";
    public static final String[] APPROVED_FORMS = {APPROVED_FORM1, APPROVED_FORM2};


    // RedcapWrapper mock results
    public static RedcapResult redcapResultForSingleId(String id) {
        List<EAVSurveyRecord> records = new ArrayList<>(3);
        for (int i=0; i < 3; i++) {
            records.add(new EAVSurveyRecord(id, "field_name_"+i, "event", "value"));
        }
         return new RedcapResult(HttpStatus.OK, records, null);
    }

    public static RedcapResult emptyRedcapResultWithStatus(HttpStatus status) {
        return new RedcapResult(status);
    }

    public static RedcapProject generateRedcapProject(int id, String url) {
        RedcapProject project = new RedcapProject();
        project.setId(id);
        project.setName(TEST_PROJECT_ALIAS);
        project.setUrl(url);
        project.setApiToken(FAKE_API_TOKEN);
        return project;
    }

    public static List<RedcapProject> getRedcapProjects(int number) {
        List<RedcapProject> projects = new ArrayList<>(number);
        for (int i=0; i < number; i++) {
            projects.add(generateRedcapProject(i, TEST_PROJECT_URL+i));
        }
        return projects;
    }
}
