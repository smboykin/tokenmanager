/**
 * Created by sboykin on 5/12/2015.
 */
var createDeferred = function(q, resolve, message) {
    var deferred = q.defer();
    if (resolve) {
        deferred.resolve(message);
    } else {
        deferred.reject(message)
    }
    return deferred;
}

