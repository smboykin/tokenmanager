/**
 * Created by sboykin on 4/17/2015.
 */

describe('SurveyManager: other controllers', function() {
    var mockAuthService;
    var fakeWindow = {
        location: {
            href: ''
        }
    }
    // setup app-level injections
    beforeEach(module('surveymanager'));

    // setup app-level injections
    beforeEach(inject(function($injector) {
        // Set up the mock http org.chip.ihl.surveymanager.service responses
        $httpBackend = $injector.get('$httpBackend');

        // Get scopes (including the root scope)
        $scope = {};
        $rootScope = $injector.get('$rootScope');
        $location = $injector.get('$location');
        $state = $injector.get('$state');
        $q = $injector.get('$q');

        mockAuthService = {};
        mockAuthService.login = function() {};
        mockAuthService.logout = function() {};

        // The $controller org.chip.ihl.surveymanager.service is used to create instances of controllers
        var $controller = $injector.get('$controller');

        createController = function(controllerName, injectionParams) {
            return $controller(controllerName, injectionParams);
        };
    }));

    // LoginController
    describe('LoginController', function() {
        var controller;
        beforeEach(function() {
            controller = createController('LoginController', {'$rootScope': $rootScope, '$scope': $scope, 'AuthService': mockAuthService, '$window': fakeWindow});
        })
        // login tests
        describe('$scope.login', function() {
            it('should be defined', function() {
                expect($scope.login).toBeDefined();
            });

            it('should login successfully', function() {
                spyOn(mockAuthService, "login").and.callFake(function() {
                    var deferred = createDeferred($q, true, "Ok");
                    return deferred.promise;
                });
                $scope.login();
                $rootScope.$apply();
                expect($rootScope.authenticated).toBe(true);
                expect($rootScope.errorMessage).toBeFalsy();
            });

            it('should fail', function() {
                spyOn(mockAuthService, "login").and.callFake(function() {
                    var deferred = createDeferred($q, false, "Login denied");
                    return deferred.promise;
                });
                $scope.login();
                $rootScope.$apply();
                expect($rootScope.authenticated).toBe(false);
                expect($rootScope.errorMessage).toBeTruthy();
            })
        });

    });

        // NavigationController
    describe('NavigationController', function() {
        var controller;
        // setup injections
        beforeEach(function() {
            // ignore any callouts to default location
            $httpBackend.when('GET', 'partials/home.html').respond(200);

            // auth request handler
            authRequestHandler = $httpBackend.when('GET', 'user').respond(200, {name: 'userX'}, {'A-Token': 'xxx'});

            createNavController = function() {
                return createController('NavigationController', {'$rootScope': $rootScope, '$scope' : $scope, '$state': $state, 'AuthService': mockAuthService, '$window': fakeWindow });
            };
            controller = createNavController();
        });

        // logout
        describe('$scope.logout', function() {
            it('should be defined', function() {
                expect($scope.logout).toBeDefined();
            });
            it('should logout successfully', function() {
                spyOn(mockAuthService, "logout").and.callFake(function() {
                    var deferred = createDeferred($q, true, "");
                    return deferred.promise;
                });
                $scope.logout();
                $rootScope.$apply();
                expect($rootScope.errorMessage).toBeFalsy();
            });
            it('should fail to logout', function() {
                spyOn(mockAuthService, "logout").and.callFake(function() {
                    var deferred = createDeferred($q, false, "Logout denied");
                    return deferred.promise;
                });
                $scope.logout();
                $rootScope.$apply();
                expect($rootScope.errorMessage).toBeTruthy();
            })
        });

    });
});
