/**
 * Created by sboykin on 4/17/2015.
 *
 * Test for controllers that handle project management
 */

describe('SurveyManager: Project controllers', function () {
    var $scope, $q, $httpBackend, $rootScope, $state, $controller, $templateCache;
    var Project, queryDeferred, deleteDeferred, saveDeferred, updateDeferred;
    var resolveThis;

    // the mock project resource org.chip.ihl.surveymanager.service, stubbing out the key functions
    var mockProject = function (properties) {
        for (var k in properties) {
            this[k] = properties[k];
        }
    }

    // setup app-level injections
    beforeEach(module('surveymanager', function ($provide) {
        $provide.value('Project', mockProject);
    }));

    beforeEach(inject(function ($injector) {
        // Set up the mock http org.chip.ihl.surveymanager.service responses
        $httpBackend = $injector.get('$httpBackend');

        $q = $injector.get('$q');
        // Get root scope
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        $state = $injector.get('$state');
        $templateCache = $injector.get('$templateCache');
        $templateCache.put('partials/projects.html', '');
        Project = $injector.get('Project');

        mockProject.query = function(resolveFn, rejectFn){
            queryDeferred = $q.defer();
            queryDeferred.promise.then(resolveFn, rejectFn);
            return queryDeferred.promise;
        };
        mockProject.get = function(params, resolveFn, rejectFn){
            queryDeferred = $q.defer();
            queryDeferred.promise.then(resolveFn, rejectFn);
            return queryDeferred.promise;
        };

        mockProject.prototype.$save = function (resolveFn, rejectFn) {
            saveDeferred = $q.defer();
            saveDeferred.promise.then(resolveFn, rejectFn);
            return saveDeferred.promise;
        };
        mockProject.prototype.$delete = function (resolveFn, rejectFn) {
            deleteDeferred = $q.defer();
            deleteDeferred.promise.then(resolveFn, rejectFn);
            return deleteDeferred.promise;
        };
        mockProject.prototype.$update = function (resolveFn, rejectFn) {
            updateDeferred = $q.defer();
            updateDeferred.promise.then(resolveFn, rejectFn);
            return updateDeferred.promise;
        };

        // The $controller org.chip.ihl.surveymanager.service is used to create instances of controllers
        $controller = $injector.get('$controller');

        createController = function (controllerName, injectionParams) {
            return $controller(controllerName, injectionParams);
        };
    }));

    // var $q, $rootScope, $scope, mockProjectResource;
    var mockPopupService;
    var mockProjectsResponse;

    beforeEach(function () {
        // ignore any callouts to default location
        $httpBackend.when('GET', 'partials/home.html').respond(200);
        $httpBackend.when('GET', 'partials/projects.html').respond(200);

        testProject1 = {
            id: 1, name: 'myProjectName_1', url: 'http://myredcap.org/myprojecturl_1', apiToken: 'myApiToken_1'
        };
        testProject2 = {
            id: 2, name: 'myProjectName_2', url: 'http://myredcap.org/myprojecturl_2', apiToken: 'myApiToken_2'
        };
        mockProjectsResponse = [testProject1, testProject2];
        $scope = $rootScope.$new();

        // the mock popup org.chip.ihl.surveymanager.service
        mockPopupService = {
            showPopup: function (value) {
                return value;
            }
        }
    });

    describe('ProjectListController', function () {
        var controller;
        beforeEach(function () {
            createPlController = function() {
                return createController('ProjectListController',
                    {'$scope': $scope, popupService: mockPopupService, '$state': $state});
            }

            spyOn(Project, 'query').and.callThrough();
            spyOn(Project.prototype, '$delete').and.callThrough();
            controller = createPlController();
        });
        it('should query the Project org.chip.ihl.surveymanager.service', function () {
            expect(Project.query).toHaveBeenCalled();
        });

        it('should successfully return list', function () {
            queryDeferred.resolve(mockProjectsResponse);
            $scope.$apply();
            expect($scope.projects).toEqual(mockProjectsResponse);
            expect($rootScope.errorMessage).not.toBeTruthy();
        });

        it('should fail to return list', function () {
            queryDeferred.reject('Some error occurred.')
            $scope.$apply();
            expect($scope.projects).toEqual([]);
            expect($rootScope.errorMessage).toBeTruthy();
        });

        describe('$scope.deleteProject', function () {
            var projectToDelete = new mockProject();
            it('should call project.$delete when confirmed', function () {
                spyOn(mockPopupService, 'showPopup').and.returnValue(true);
                $scope.deleteProject(projectToDelete);
                expect(Project.prototype.$delete).toHaveBeenCalled();
            });
            it('should successfully delete a project when confirmed', function () {
                spyOn(mockPopupService, 'showPopup').and.returnValue(true);
                $scope.deleteProject(projectToDelete);
                deleteDeferred.resolve(204);
                $rootScope.$apply();
                expect($rootScope.errorMessage).not.toBeTruthy();
                console.log('State is: ' + $state.current.name)
                expect($state.is('projects')).toBe(true);
            });
            it('should fail to delete a project when confirmed', function () {
                spyOn(mockPopupService, 'showPopup').and.returnValue(true);
                $scope.deleteProject(projectToDelete);
                deleteDeferred.reject('Server rejected it for some reason');
                $rootScope.$apply();
                expect($rootScope.errorMessage).toBeTruthy();
            });
            it('should do nothing when not confirmed', function () {
                spyOn(mockPopupService, 'showPopup').and.returnValue(false);
                $scope.deleteProject(projectToDelete);
                $rootScope.$apply();
                expect(Project.prototype.$delete).not.toHaveBeenCalled();
            });
        });
    });
    describe('ProjectCreateController', function () {
        var controller;
        beforeEach(function () {
            controller = createController('ProjectCreateController', {
                '$scope': $scope, popupService: mockPopupService
            });
            spyOn(Project.prototype, '$save').and.callThrough();
        });

    // TODO Unit tests for addForm, deleteForm
    //    describe('$scope.addForm', function() {
    //        it('should call formService.addForm ', function () {
    //            $scope.addProject();
    //            expect()
    //        })
    //    });
        it('should have empty project.forms ', function () {
            expect(Project.prototype.forms).toBeFalsy();
        });

        describe('$scope.addProject', function () {
            it('should call project.$save ', function () {
                $scope.addProject();
                expect(Project.prototype.$save).toHaveBeenCalled();
            });
            it('should successfully save a new project', function () {
                $scope.addProject();
                saveDeferred.resolve({});
                $rootScope.$apply();
                expect($rootScope.errorMessage).not.toBeTruthy();
                expect($state.is('projects')).toBe(true);
                expect($state.params.appMessage).toBeTruthy();
            });
            it('should fail to save a project', function () {
                $scope.addProject();
                saveDeferred.reject('Server rejected it for some reason');
                $rootScope.$apply();
                expect($rootScope.errorMessage).toBeTruthy();
            });
        });

    });
    describe('ProjectViewController', function () {
        var controller;
        beforeEach(function () {
            spyOn(Project, 'get').and.callThrough();
            controller = createController('ProjectViewController', {
                '$scope': $scope, popupService: mockPopupService
            });
        });

        it('should call Project.get', function () {
            $rootScope.$apply();
            expect(Project.get).toHaveBeenCalled();
        });

        it('should successfully return project', function () {
            queryDeferred.resolve(testProject1);
            $rootScope.$apply();
            expect($scope.project).toEqual(testProject1);
            expect($rootScope.errorMessage).not.toBeTruthy();
        });

        it('should fail to return project', function () {
            queryDeferred.reject('Some server problem');
            $rootScope.$apply();
            expect($scope.project).toEqual({});
            expect($rootScope.errorMessage).toBeTruthy();
            expect($rootScope.appMessage).not.toBeTruthy();
        });

    });
    describe('ProjectEditController', function () {
        var controller;
        beforeEach(function () {
            spyOn(Project, 'get').and.callThrough();
            spyOn(Project.prototype, '$update').and.callThrough();
            controller = createController('ProjectEditController', {
                '$scope': $scope, popupService: mockPopupService
            });
        });
        describe('$scope.loadProject', function() {
            it('should call Project.get', function() {
                $rootScope.$apply();
                expect(Project.get).toHaveBeenCalled();
            });

            it('should successfully return project', function () {
                queryDeferred.resolve(testProject1);
                $rootScope.$apply();
                expect($scope.project).toEqual(testProject1);
                expect($rootScope.errorMessage).not.toBeTruthy();
            });

            it('should fail to return project', function () {
                queryDeferred.reject('Some server problem');
                $rootScope.$apply();
                expect($scope.project).toEqual({});
                expect($rootScope.errorMessage).toBeTruthy();
            });
        });
        describe('$scope.updateProject', function() {
            beforeEach(function() {
                $scope.project = new mockProject();
            });
            it('should call Project.update', function() {
                $scope.updateProject();
                updateDeferred.resolve();
                $rootScope.$apply();
                expect(Project.prototype.$update).toHaveBeenCalled();
            });

            it('should successfully update project', function () {
                $scope.updateProject();
                updateDeferred.resolve();
                $rootScope.$apply();
                expect($rootScope.errorMessage).not.toBeTruthy();
                expect($state.is('projects')).toBe(true);
                expect($state.params.appMessage).toBeTruthy();
            });

            it('should fail to update project', function () {
                $scope.updateProject();
                updateDeferred.reject('Some server problem');
                $rootScope.$apply();
                expect($rootScope.errorMessage).toBeTruthy();
            });
        });
    });

});
